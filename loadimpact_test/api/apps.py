from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class APIConfig(AppConfig):
    name = "loadimpact_test.api"
    verbose_name = _("API")

    def ready(self):
        try:
            # pylint: disable=unused-import,import-outside-toplevel
            import loadimpact_test.api.signals  # type: ignore
        except ImportError:
            pass
