# pylint: disable=abstract-method
from rest_framework import serializers


class DataCenterSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)  # type: ignore
    servers = serializers.IntegerField()  # type: ignore


class InputSerializer(serializers.Serializer):
    DM_capacity = serializers.IntegerField()  # type: ignore
    DE_capacity = serializers.IntegerField()  # type: ignore
    data_centers = DataCenterSerializer(many=True)
