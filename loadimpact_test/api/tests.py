# pylint: disable=redefined-outer-name
import pytest  # type: ignore
from django.urls import reverse
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.test import APIClient

pytestmark = pytest.mark.django_db


@pytest.fixture
def api_client():
    return APIClient()


@pytest.mark.parametrize(
    "payload, expected_result",
    [
        pytest.param(
            {
                "DM_capacity": 20,
                "DE_capacity": 8,
                "data_centers": [
                    {"name": "Paris", "servers": 20},
                    {"name": "Stockholm", "servers": 62},
                ],
            },
            {"DE": 8, "DM_data_center": "Paris"},
            id="1",
        ),
        pytest.param(
            {
                "DM_capacity": 6,
                "DE_capacity": 10,
                "data_centers": [
                    {"name": "Paris", "servers": 30},
                    {"name": "Stockholm", "servers": 66},
                ],
            },
            {"DE": 9, "DM_data_center": "Stockholm"},
            id="2",
        ),
        pytest.param(
            {
                "DM_capacity": 12,
                "DE_capacity": 7,
                "data_centers": [
                    {"name": "Berlin", "servers": 11},
                    {"name": "Stockholm", "servers": 21},
                ],
            },
            {"DE": 3, "DM_data_center": "Berlin"},
            id="3",
        ),
    ],
)
def test_api(payload, expected_result, api_client):

    res = api_client.post(reverse("api"), payload, format="json")
    assert res.status_code == HTTP_200_OK
    assert res.json() == expected_result


def test_invalid_payloads(api_client):
    res = api_client.post(reverse("api"), {"invalid": "payload"}, format="json")
    assert res.status_code == HTTP_400_BAD_REQUEST
