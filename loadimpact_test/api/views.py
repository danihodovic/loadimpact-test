import math

from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from loadimpact_test.api.serializers import InputSerializer


class SchedulerView(APIView):
    def post(self, request, format=None):  # pylint: disable=redefined-builtin
        serializer = InputSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

        result = _calculate(serializer.validated_data)
        return Response(result)


def _calculate(data):
    solutions = []

    # Iterate over all possible placements of the DevOps manager
    for idx, _ in enumerate(data["data_centers"]):
        dm_data_center = data["data_centers"][idx]
        solutions.append(
            {
                "DM_data_center": dm_data_center["name"],
                "DE": _required_devops_engineers(data, idx),
            }
        )

    # Find the solution that required the least amount of DevOps engineers
    return min(solutions, key=lambda s: s["DE"])


def _required_devops_engineers(data, de_placement_idx):
    required_dm = 0
    for idx, dc in enumerate(data["data_centers"]):
        servers = dc["servers"]
        if de_placement_idx == idx:
            servers -= min(data["DM_capacity"], dc["servers"])
        required_dm += math.ceil(servers / data["DE_capacity"])

    return required_dm
